package com.example.lukaszwolinski.databinding_example;

import android.content.Context;

import com.example.lukaszwolinski.databinding_example.model.Person;
import com.example.lukaszwolinski.databinding_example.model.PersonService;
import com.example.lukaszwolinski.databinding_example.viewmodel.PersonViewModel;

import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PersonViewModelUnitTest {

    @Test
    public void testSubmitPerson_WhenDefaultPersonReturnedFromGetPerson_SubmitsDefaultPerson() {
        //Arrange
        Person person = new Person("Tadeusz", "Kościuszko");
        PersonService personService = mock(PersonService.class);
        when(personService.getPerson()).thenReturn(person);

        Context context = mock(Context.class);
        PersonViewModel personViewModelUnderTest = new PersonViewModel(personService, context);

        //Act
        personViewModelUnderTest.submitPerson();

        //Assert
        verify(personService, times(1)).submitPerson(person);
    }
}

package com.example.lukaszwolinski.databinding_example.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.lukaszwolinski.databinding_example.viewmodel.PersonListViewModel;
import com.example.lukaszwolinski.databinding_example.model.PersonService;
import com.example.lukaszwolinski.databinding_example.R;
import com.example.lukaszwolinski.databinding_example.databinding.ActivityPersonListBinding;

public class PersonListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityPersonListBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_person_list);
        binding.setViewModel(new PersonListViewModel(new PersonService()));
    }
}

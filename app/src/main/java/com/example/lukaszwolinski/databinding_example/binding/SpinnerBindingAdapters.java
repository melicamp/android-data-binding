package com.example.lukaszwolinski.databinding_example.binding;

import android.databinding.BindingAdapter;
import android.databinding.InverseBindingAdapter;
import android.databinding.InverseBindingListener;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

public class SpinnerBindingAdapters {

    @InverseBindingAdapter(attribute = "android:selectedItem")
    public static int getSelectedItem(Spinner view) {
        return view.getSelectedItemPosition();
    }

    @BindingAdapter("android:selectedItem")
    public static void setSelectedItem(Spinner view, int itemPosition) {
        if (itemPosition != view.getSelectedItemPosition()) {
            view.setSelection(itemPosition);
        }
    }

    @BindingAdapter("android:selectedItemAttrChanged")
    public static void setItemChangedListener(Spinner view,
                                              final InverseBindingListener selectedItemChanged) {
        if (selectedItemChanged == null) {
            view.setOnItemSelectedListener(null);
        } else {
            view.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    selectedItemChanged.onChange();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }
}

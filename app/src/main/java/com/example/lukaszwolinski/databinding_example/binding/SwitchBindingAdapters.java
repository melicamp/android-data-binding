package com.example.lukaszwolinski.databinding_example.binding;

import android.databinding.BindingAdapter;
import android.widget.CompoundButton;
import android.widget.Switch;

public class SwitchBindingAdapters {

    @BindingAdapter("android:onCheckedChanged")
    public static void setOnCheckedChangedListener(Switch view, CompoundButton.OnCheckedChangeListener checkedChangeListener){
        view.setOnCheckedChangeListener(checkedChangeListener);
    }

}

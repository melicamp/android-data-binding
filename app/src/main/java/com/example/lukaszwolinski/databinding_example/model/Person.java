package com.example.lukaszwolinski.databinding_example.model;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.example.lukaszwolinski.databinding_example.BR;

public class Person extends BaseObservable {

    @Bindable
    private String firstName;
    @Bindable
    private String lastName;
    @Bindable
    private Gender gender = Gender.MALE;
    @Bindable
    private String genderText;

    public String getGenderText() {
        return gender.getStringValue();
    }

    public Person(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public int getGender() {
        return gender.ordinal();
    }

    public void setGender(int gender) {
        this.gender = Gender.getGenderForInt(gender);
        notifyPropertyChanged(BR.gender);
        notifyPropertyChanged(BR.genderText);
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
        notifyPropertyChanged(BR.firstName);
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
        notifyPropertyChanged(BR.lastName);
    }

    @Override
    public String toString() {
        return firstName + " " + lastName;
    }
}

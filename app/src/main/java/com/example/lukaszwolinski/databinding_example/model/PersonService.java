package com.example.lukaszwolinski.databinding_example.model;

import java.util.ArrayList;
import java.util.List;

public class PersonService {

    /**
     * Get Person object from server.
     * @return
     */
    public Person getPerson(){
        Person person = new Person("Jan", "Kowalski");

        return person;
    }

    /**
     * Get list of Person objects from server.
     * @return
     */
    public List<Person> getPeople(){
        List<Person> people = new ArrayList<>();

        people.add(new Person("Andrzej", "Nowak"));
        people.add(new Person("Marek", "Kafarek"));
        people.add(new Person("Ziemowit", "Podłużny"));
        people.add(new Person("Andrzej", "Nowak"));
        people.add(new Person("Marek", "Kafarek"));
        people.add(new Person("Ziemowit", "Podłużny"));
        people.add(new Person("Andrzej", "Nowak"));
        people.add(new Person("Marek", "Kafarek"));
        people.add(new Person("Ziemowit", "Podłużny"));
        people.add(new Person("Andrzej", "Nowak"));
        people.add(new Person("Marek", "Kafarek"));
        people.add(new Person("Ziemowit", "Podłużny"));
        people.add(new Person("Andrzej", "Nowak"));
        people.add(new Person("Marek", "Kafarek"));
        people.add(new Person("Ziemowit", "Podłużny"));
        people.add(new Person("Andrzej", "Nowak"));
        people.add(new Person("Marek", "Kafarek"));
        people.add(new Person("Ziemowit", "Podłużny"));
        people.add(new Person("Andrzej", "Nowak"));
        people.add(new Person("Marek", "Kafarek"));
        people.add(new Person("Ziemowit", "Podłużny"));
        people.add(new Person("Andrzej", "Nowak"));
        people.add(new Person("Marek", "Kafarek"));
        people.add(new Person("Ziemowit", "Podłużny"));

        return people;
    }

    /**
     * Submit person object to server.
     * @param person
     */
    public void submitPerson(Person person) {
        //Call server and post person object.
    }
}

package com.example.lukaszwolinski.databinding_example.viewmodel;

import android.content.Context;
import android.content.Intent;
import android.databinding.ObservableBoolean;
import android.widget.Toast;

import com.example.lukaszwolinski.databinding_example.model.Person;
import com.example.lukaszwolinski.databinding_example.model.PersonService;
import com.example.lukaszwolinski.databinding_example.activity.PersonListActivity;

public class PersonViewModel {

    public final ObservableBoolean showEditSection = new ObservableBoolean(false);
    private final Context context;
    private final PersonService personService;
    private Person person;

    public PersonViewModel(PersonService personService, Context context) {
        this.personService = personService;
        this.person = personService.getPerson();
        this.context = context;
    }

    public Person getPerson() {
        return person;
    }

    public void onWhatIsMyNameButtonClicked(){
        Toast.makeText(context, getFullName(), Toast.LENGTH_LONG).show();
    }

    public void goToPersonList(){
        Intent intent = new Intent(context, PersonListActivity.class);
        context.startActivity(intent);
    }

    public void submitPerson(){
        personService.submitPerson(person);
        Toast toast = Toast.makeText(context, "Zapisuję na serwerze...", Toast.LENGTH_LONG);
        if(toast != null){
            toast.show();
        }
    }

    public void showEditSection(boolean shouldShow){
        showEditSection.set(shouldShow);
    }

    private String getFullName() {
        return String.format(
                "Jestem %s %s",
                person.getFirstName(),
                person.getLastName());
    }
}

package com.example.lukaszwolinski.databinding_example.viewmodel;

import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;

import com.example.lukaszwolinski.databinding_example.BR;
import com.example.lukaszwolinski.databinding_example.model.Person;
import com.example.lukaszwolinski.databinding_example.model.PersonService;
import com.example.lukaszwolinski.databinding_example.R;

import me.tatarka.bindingcollectionadapter2.ItemBinding;

public class PersonListViewModel {
    public final ObservableList<Person> items = new ObservableArrayList<>();
    public final ItemBinding<Person> itemBinding = ItemBinding.of(BR.item, R.layout.person_item);

    public PersonListViewModel(PersonService personService) {
        items.addAll(personService.getPeople());
    }
}

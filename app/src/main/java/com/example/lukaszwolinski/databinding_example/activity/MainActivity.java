package com.example.lukaszwolinski.databinding_example.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.lukaszwolinski.databinding_example.model.PersonService;
import com.example.lukaszwolinski.databinding_example.viewmodel.PersonViewModel;
import com.example.lukaszwolinski.databinding_example.R;
import com.example.lukaszwolinski.databinding_example.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.setViewModel(new PersonViewModel(new PersonService(), this));
    }
}

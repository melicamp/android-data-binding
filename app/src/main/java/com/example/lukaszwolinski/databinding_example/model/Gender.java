package com.example.lukaszwolinski.databinding_example.model;

public enum Gender {
    MALE("Mężczyzna"),
    FEMALE("Kobieta");

    private String stringValue;

    public String getStringValue() {
        return stringValue;
    }

    Gender(String stringValue) {
        this.stringValue = stringValue;
    }

    public static Gender getGenderForInt(int intValue) {
        if (FEMALE.ordinal() == intValue) {
            return FEMALE;
        } else if (MALE.ordinal() == intValue) {
            return MALE;
        } else {
            throw new IllegalStateException("Unknown gender.");
        }
    }
}
